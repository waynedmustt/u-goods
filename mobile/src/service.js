import { config } from './config';

export function hasPermission(resource, operation) {
    if (!resource || !operation) {
        return false;
    }

    if (!config.activeUser) {
        return false;
    }

    const type = config.activeUser.type;
    if (!config.acl[type]) {
        return false;
    }

    let permission = false;

    config.acl[type].filter(function (data) {
        if (!config.acl[data] || !config.acl[data][resource]) {
            permission = false;
            return permission;
        }
        
        permission = config.acl[data][resource].indexOf(operation) !== -1
    });

    return permission;

} 