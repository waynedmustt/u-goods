import React from 'react';
import loginScreen from '../screens/login';
import goodsScreen from '../screens/goods';
import accountScreen from '../screens/account';
import notificationScreen from '../screens/notification';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import FooterMenu from '../screens/footer-menu';

export const tabs = createBottomTabNavigator({
  Goods: goodsScreen,
  Notification: notificationScreen,
  Account: accountScreen
},
{
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  tabBarComponent: props => {
    return (
      <FooterMenu
      params={props}
      />
    );
  }
}
);

export const MainRoute = createStackNavigator(
    {
      Login: loginScreen,
      Goods: goodsScreen,
      Tabs: tabs
    },
    {
      initialRouteName: 'Login',
      navigationOptions: {
        header: null
       }
    }
  );