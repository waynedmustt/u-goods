export const config = {
    timer: 30,
    timerUnit: 'detik',
    user: [
        {
            username: 'driver',
            password: '123',
            type: 'driver',
        },
        {
            username: 'manager',
            password: 'manager',
            type: 'manager',
        }
    ],
    acl: {
        goodsDriverRole: {
            goods: ['view'],
            account: ['view'],
            menuList: ['view']
        },
        goodsManagerRole: {
            goods: ['view', 'add', 'edit'],
            notification: ['view', 'add', 'edit'],
            account: ['view']
        },
        driver: ['goodsDriverRole'],
        manager: ['goodsManagerRole']
    },
    footerTabList: [
        {
            id: 'goods',
            icon: 'briefcase',
            text: 'Barang',
            route: 'Goods'
        },
        {
            id: 'notification',
            icon: 'notifications',
            text: 'Notifikasi',
            route: 'Notification'
        },
        {
            id: 'account',
            icon: 'person',
            text: 'Akun',
            route: 'Account'
        }
    ],
    activeUser: {}
};