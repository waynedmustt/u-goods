import React, { Component } from 'react';
import {
    Container,
    Header,
    Body,
    Title,
    Button,
    Left,
    Right,
    Icon,
    StyleProvider } from 'native-base';
import getTheme from '../../../native-base-theme/components';
import material from '../../../native-base-theme/variables/material';

class Layout extends Component {
    constructor(props) {
        super(props);
        if (this.props.renderHeader.left && typeof this.props.renderHeader.left.callback !== 'function') {
            this.props.renderHeader.left.callback = () => function () {};
        }

        if (this.props.renderHeader.right && typeof this.props.renderHeader.right.callback !== 'function') {
            this.props.renderHeader.right.callback = () => function () {};
        }

        if (this.props.renderHeader.left 
            && typeof this.props.renderHeader.body.permissions === 'undefined') {
            this.props.renderHeader.left.permissions = true;
        }

        if (this.props.renderHeader.body 
            && typeof this.props.renderHeader.body.permissions === 'undefined') {
            this.props.renderHeader.body.permissions = true;
        }

        if (this.props.renderHeader.right 
            && typeof this.props.renderHeader.right.permissions === 'undefined') {
            this.props.renderHeader.right.permissions = true;
        }
    }

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
            <Container>
                <Header>
                {this.props.renderHeader.left && this.props.renderHeader.left.permissions ?
                <Left>
                    <Button transparent onPress={
                        this.props.renderHeader.left.callback.bind()
                    }>
                    <Icon name={this.props.renderHeader.left.icon} />
                    </Button>
                </Left> : null
                }
                {this.props.renderHeader.body && this.props.renderHeader.body.permissions ?
                <Body>
                    <Title>{this.props.renderHeader.body.title}</Title>
                </Body> : null
                }
                {this.props.renderHeader.right && this.props.renderHeader.right.permissions ?
                <Right>
                    <Button transparent onPress={
                        this.props.renderHeader.right.callback.bind()
                    }>
                    <Icon name={this.props.renderHeader.right.icon} />
                    </Button>
                </Right> : null
                }
                </Header>
                {this.props.children}
            </Container>
            </StyleProvider>
        );
    }
}

export default Layout;