import React, { Component } from 'react';
import {
    Text, 
    List, 
    ListItem, 
    Body, 
    Right, 
    Button, 
    Icon,
    Left,
    Content
} from 'native-base';
import Layout from '../../components/layout';
class Notification extends Component {
    constructor(props) {
        super(props);
        this.renderHeader = {
            body: {
                title: 'U-Goods'
            },
            right: {

            }
        }
        this.notifData = [
            {title: 'Barang Rusak Terdeteksi', 
            description: 'Barang ID: 405D, Nama Barang: Mangkok', time: '4:50pm', notReaded: true},
            {title: 'Barang Rusak Terdeteksi', 
            description: 'Barang ID: 576E, Nama Barang: Piring', time: '2:33pm', notReaded: true},
            {title: 'Barang Rusak Terdeteksi', 
            description: 'Barang ID: 894M, Nama Barang: Piring kecil', time: '1:48pm', notReaded: false},
            {title: 'Barang Rusak Terdeteksi', 
            description: 'Barang ID: 778Z, Nama Barang: Gelas Kecil', time: 'yesterday', notReaded: false},
            {title: 'Barang Rusak Terdeteksi', 
            description: 'Barang ID: 678G, Nama Barang: Gelas Kaca', time: '2 days ago', notReaded: false}
        ];
    }

    render() {
        return (
            <Layout
            renderHeader={this.renderHeader}
            >
                <Content>
                    {this.notifData.map((data, i) => 
                        (
                        <List key={i}>
                            <ListItem avatar>
                                {data.notReaded ? 
                                <Left>
                                    <Icon name='information-circle' style={{color: 'red'}}></Icon>
                                </Left>    
                                : <Left />}
                                <Body>
                                    <Text style={[{
                                        color: 'red'}, 
                                        data.notReaded ? {fontWeight: 'bold'}: null
                                        ]}>{data.title}</Text>
                                    <Text style={data.notReaded ? {fontWeight: 'bold'} : null} note={!data.notReaded}>
                                        {data.description}
                                    </Text>
                                </Body>
                                <Right>
                                    <Text style={data.notReaded ? {fontWeight: 'bold'} : null} note={!data.notReaded}>
                                        {data.time}
                                    </Text>
                                </Right>
                            </ListItem>
                        </List>
                        )
                    )}
                </Content>
            </Layout>
        );
    }
}

export default Notification;