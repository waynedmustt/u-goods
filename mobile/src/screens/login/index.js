import React, { Component } from 'react';
import {
  Body,
  Content,
  Button,
  Text,
  Card,
  CardItem,
  Item,
  Input } from 'native-base';
import styles from './styles';
import Layout from '../../components/layout';
import {config} from '../../config';
import { NavigationActions } from 'react-navigation';

class Login extends Component {
    constructor(props) {
        super(props);
        this.renderHeader = {
            body: {
                title: 'U-Goods'
            },
            right: {

            }
        }
        this.state = {
            username: '',
            password: '',
            hasError: false,
            errorMessage: ''
        };
    }

    submit() {
        if (!config || !config.user) {
            this.setState({hasError: true});
            this.setState({errorMessage: 'username or password does not exist!'});
            return;
        }

        if (!this.state.username || !this.state.password) {
            this.setState({hasError: true});
            this.setState({errorMessage: 'username or password is empty'});
            return;
        }
        
        const matchUser = config.user.filter((data) => 
        data.username === this.state.username &&
        data.password === this.state.password
        );

        if (matchUser && matchUser.length === 0) {
            this.setState({errorMessage: 'username or password does not match!'});
            this.setState({hasError: matchUser && matchUser.length === 0});
            return;
        }

        const goodsNavigation = NavigationActions.navigate({
            routeName: 'Tabs'
        });
        config.activeUser = matchUser[0];
        this.props.navigation.dispatch(goodsNavigation);
    }

    render() {
        return (
            <Layout
            renderHeader={this.renderHeader}
            >
                <Content contentContainerStyle={styles.content}>
                    <Card transparent>
                    <CardItem>
                        <Body style={styles.body}>
                        <Item style={styles.item}>
                            <Input placeholder='Name' 
                            onChangeText={(value) => this.setState({username: value})}
                            />
                        </Item>

                        <Item style={[styles.item, styles.mt]}>
                            <Input placeholder='Password' 
                            onChangeText={(value) => this.setState({password: value})}
                            secureTextEntry={true}/>
                        </Item>
                        {this.state.hasError ? 
                        <Text style={styles.errorText}>
                            {this.state.errorMessage}
                        </Text> : null
                        }
                        <Button style={[styles.button, styles.mt]} block
                            onPress={() => this.submit()}>
                            <Text>Login</Text>
                        </Button>

                        <Item style={[styles.item, styles.mt, styles.noBorder]}>
                            <Text style={styles.text}>Forgot your password?</Text>
                        </Item>
                        </Body>
                    </CardItem>
                    </Card>
                </Content>
            </Layout>
        );
    }
}

export default Login;