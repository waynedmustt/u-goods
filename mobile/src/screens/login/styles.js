import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    content: {flex:1, justifyContent: 'center'},
    body: {flex: 1, alignItems: 'center'},
    item: {width: 300},
    button: {width: 300, alignSelf:'center'},
    mt: {marginTop: 20},
    noBorder: {backgroundColor: 'transparent', borderColor: 'rgba(255,255,255,.4)'},
    text: {width: 290, textAlign: 'center', lineHeight:13, fontSize:13 },
    errorText: {color: 'red', fontSize: 14, marginTop: 2}
  });