import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    dashboard: {
        flexDirection: 'row', 
        width: 350, 
        alignSelf: 'center',
        borderRadius: 4,
        marginTop: 10,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 1
    },
    timer: {
        width: 350,
        alignSelf: 'center',
        borderRadius: 4,
        marginTop: 10
    },
    padding: {paddingTop: 5, paddingBottom: 5},
    timerColor: {backgroundColor: '#00CCCC'},  
    goodColor: {backgroundColor: 'green'},
    badColor: {backgroundColor: 'red'},
    warningColor: {backgroundColor: 'orange'}
  });