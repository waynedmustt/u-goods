import React, { Component } from 'react';
import Layout from '../../components/layout';
import { 
    Text, 
    List, 
    ListItem, 
    Body, 
    Right, 
    Button, 
    Icon,
    View,
    Content,
    Spinner, 
    ActionSheet
} from 'native-base';
import { showMessage } from 'react-native-flash-message';
import styles from './styles';
import { config } from '../../config';
import { hasPermission } from '../../service';
import { Alert } from 'react-native';

var BUTTONS = ['Laporkan', 'Batal'];
var CANCEL_INDEX = 1;

class Goods extends Component {
    constructor(props) {
        super(props);

        this.renderHeader = {
            left: {
                icon: 'menu'
            },
            body: {
                title: 'U-Goods'
            },
            right: {
            }
        };
        this.goodsData = [
            {label: 'Gelas Kaca', height: '14', sizeUnit: 'cm', diameter: '22', 
            temperature: '19', temperatureUnit: 'C', status: 'goodCondition', goodsId: '405D'},
            {label: 'Mangkok', height: '5', sizeUnit: 'cm', diameter: '18', 
            temperature: '25', temperatureUnit: 'C', status: 'goodCondition', goodsId: '785E'},
            {label: 'Piring', height: '4', sizeUnit: 'cm', diameter: '9', 
            temperature: '22', temperatureUnit: 'C', status: 'broken', goodsId: '987F'},
            {label: 'Botol Wine', height: '31', sizeUnit: 'cm', diameter: '7', 
            temperature: '32', temperatureUnit: 'C', status: 'needToCheck', goodsId: '247Y'},
            {label: 'Piring Makan', height: '4', sizeUnit: 'cm', diameter: '9', 
            temperature: '14', temperatureUnit: 'C', status: 'goodCondition', goodsId: '556R'},
            {label: 'Piring Makan Kecil', height: '4', sizeUnit: 'cm', diameter: '5', 
            temperature: '13', temperatureUnit: 'C', status: 'broken', goodsId: '178S'}
        ];
        this.state = {
            loading: true,
            timer: config.timer || 30,
            goodsData: this.goodsData,
            goodCondition: 0,
            broken: 0,
            needToCheck: 0
        };
        this.statusData = {
            goodCondition: {value: 'kondisi bagus', color: 'green'},
            broken: {value: 'rusak', color: 'red'},
            needToCheck: {value: 'perlu di periksa', color: 'orange'}
        };
        showMessage({
            message: 'PERINGATAN!',
            description: 'Status dari kondisi barang akan terbaharui setiap 10 menit, harap diperhatikan!',
            duration: '4000',
            autoHide: false,
            type: 'warning'
        });
        this.startTimer();
        this.polling = 0;
    }

    startTimer = () => {
        this.clockCall = setInterval(() => {
            this.decrementClock();
        }, 1000);
    }

    decrementClock = () => {  
        if(this.state.timer === 0) {
            this.polling++;
            this.state.timer = config.timer || 30
            if (this.polling === 1) {
                // change the status data
                this.goodsData[1].status = 'broken';
                this.reCountGoods();
            }

            if (this.polling === 2) {
                // change the status data
                this.goodsData[0].status = 'needToCheck';
                this.reCountGoods();
            }
        }
        this.setState((prevstate) => ({ timer: prevstate.timer-1 }));
    };
    
    componentWillUnmount() {
        clearInterval(this.clockCall);
    }

    componentWillMount() {
        this.countGoods('goodCondition');
        this.countGoods('broken');
        this.countGoods('needToCheck');
        this.setState({ loading: false });
    }

    setButtonIndex(index) {
        this.setState({clicked: BUTTONS[index]});
        switch(BUTTONS[index]) {
         case 'Laporkan':
            Alert.alert(
                '',
                'Anda yakin untuk melaporkan barang ini?',
                [
                {text: 'Batal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Lapor', onPress: () => 
                    showMessage({
                        message: 'Berhasil!',
                        description: 'Barang berhasil dilaporkan!',
                        duration: '4000',
                        type: 'success'
                    })
                }
                ],
                { cancelable: false }
            )
         break;
        }
    }

    countGoods(goodsStatus) {
        const selectedGoods = this.goodsData.filter((data) => data.status === goodsStatus );
        switch(goodsStatus) {
            case 'goodCondition':
                this.setState({goodCondition: selectedGoods.length})
            break;
            case 'broken':
                this.setState({broken: selectedGoods.length})
            break;
            case 'needToCheck':
                this.setState({needToCheck: selectedGoods.length})
            break;
        }
    }

    reCountGoods() {
        this.countGoods('goodCondition');
        this.countGoods('broken');
        this.countGoods('needToCheck');
    }

    render() {
        const self = this;
        return (
            <Layout
            renderHeader={this.renderHeader}
            >
            {this.state.loading ? <Spinner color='black'/> : 
            <View>
                <View style={[styles.timer, styles.timerColor]}>
                    <Text
                    style={[{color: 'white', fontSize: 18, alignSelf: 'center'}, styles.padding]}
                    >
                        status akan terbaharui dalam {this.state.timer} {config.timerUnit || 'detik'}
                    </Text>
                </View>
                <View style={[styles.dashboard, styles.goodColor, styles.padding]}>
                    <View style={{flex: 3, marginLeft: 20}}>
                        <Icon name='checkmark' style={{fontSize: 52, color: 'white', marginTop: -2}}></Icon>
                        <Text style={{color: 'white', marginTop: -10}}>Kondisi bagus</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={{fontSize: 60, color: 'white'}}>{this.state.goodCondition}</Text>
                    </View>
                </View>
                <View style={[styles.dashboard, styles.badColor, styles.padding]}>
                    <View style={{flex: 3, marginLeft: 20}}>
                        <Icon name='close-circle' style={{fontSize: 52, color: 'white', marginTop: -2}}></Icon>
                        <Text style={{color: 'white', marginTop: -10}}>Rusak</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={{fontSize: 60, color: 'white'}}>{this.state.broken}</Text>
                    </View>
                </View>
                <View style={[styles.dashboard, styles.warningColor, styles.padding]}>
                    <View style={{flex: 3, marginLeft: 20}}>
                        <Icon name='information-circle' style={{fontSize: 52, color: 'white', marginTop: -2}}></Icon>
                        <Text style={{color: 'white', marginTop: -10}}>Perlu di periksa</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={{fontSize: 60, color: 'white'}}>{this.state.needToCheck}</Text>
                    </View>
                </View>
            </View>
            }
            {this.state.loading ? null :
                <Content>
                    {this.goodsData.map( (data, i) => 
                        (<List key={i}>
                            <ListItem>
                                <Body>
                                    <Text>{data.label}</Text>
                                    <Button transparent>
                                        <Text note>{data.goodsId}</Text>
                                    </Button>
                                    <Text note>{data.height} {data.sizeUnit}</Text>
                                    <Button transparent>
                                        <Icon name='cloud'></Icon>
                                        <Text style={{marginLeft: -20}} note>{data.temperature} &#8451;</Text>
                                    </Button>
                                    <Text
                                    style={{color: this.statusData[data.status].color}}
                                    >{this.statusData[data.status].value}</Text>
                                </Body>
                                <Right>
                                    {hasPermission('menuList', 'view') ? 
                                    <Button transparent onPress={() => 
                                        ActionSheet.show(
                                            {
                                                options: BUTTONS,
                                                cancelButtonIndex: CANCEL_INDEX,
                                                title: 'Tindakan'
                                            },
                                            buttonIndex => {
                                                self.setButtonIndex(buttonIndex);
                                            }
                                        )
                                    }>
                                        <Icon name='more'></Icon>
                                    </Button> : null
                                    }
                                </Right>
                            </ListItem>
                        </List>
                        )
                    )}
                </Content>
            }
            </Layout>
        );
    }
}

export default Goods;