import React, {Component} from 'react';
import {config} from '../../config';
import {
  Footer,
  FooterTab,
  Button,
  Icon,
  Text,
  StyleProvider,
  Badge
} from 'native-base';
import getTheme from '../../../native-base-theme/components';
import material from '../../../native-base-theme/variables/material';
import { hasPermission } from '../../service';

class FooterMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const self = this.props.params;
        const newFooterTabList = config.footerTabList.filter((data) => hasPermission(data.id, 'view'))
        return(
            <StyleProvider style={getTheme(material)}>
                <Footer>
                    <FooterTab>
                    {newFooterTabList.map( (data, i) =>
                        (<Button key={i} active={self.navigation.state.index === i} 
                        badge={data.id === 'notification' && hasPermission(data.id, 'view')}
                        onPress={
                            () => self.navigation.navigate(data.route)
                        }
                        >
                            {data.id === 'notification' && hasPermission(data.id, 'view') ?
                                <Badge><Text>2</Text></Badge> : null
                            }
                            <Icon name={data.icon}></Icon>
                            <Text>{data.text}</Text>
                            </Button>
                        )
                        )
                    }
                    </FooterTab>
                </Footer>
            </StyleProvider>
        );
    }
}

export default FooterMenu;