import React, { Component } from 'react';
import {
    Content, ListItem, Left, Button, Icon, Body, Text } from 'native-base';
import Layout from '../../components/layout';
import {config} from '../../config';
import { NavigationActions } from 'react-navigation';

class Account extends Component {
    constructor(props) {
        super(props);
        this.renderHeader = {
            body: {
                title: 'U-Goods'
            },
            right: {

            }
        }
    }

    logout() {
        const loginNavigation = NavigationActions.navigate({
            routeName: 'Login'
        });
        config.activeUser = {};
        this.props.navigation.dispatch(loginNavigation);
    }

    render() {
        return (
            <Layout
            renderHeader={this.renderHeader}
            >
                <Content>
                    <ListItem icon>
                        <Left>
                            <Button 
                            onPress={() => this.logout()}
                            >
                                <Icon name='log-out'></Icon>
                            </Button>
                        </Left>
                        <Body>
                            <Text>Log-out</Text>
                        </Body>
                    </ListItem>
                </Content>
            </Layout>
        );
    }
}

export default Account;