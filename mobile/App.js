import React, { Component } from 'react';
import { Font, AppLoading } from 'expo';
import { Root } from 'native-base';
import { MainRoute } from './src/navigators';
import FlashMessage from "react-native-flash-message";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
  }

  async componentWillMount() {
    await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require('native-base/Fonts/Ionicons.ttf'),
        FontAwesome: require('native-base/Fonts/FontAwesome.ttf'),
    });
    this.setState({ loading: false });
}

  render() {
    if (this.state.loading) {
      return (
        <Root>
          <AppLoading />
        </Root>
      );
    }

    return (
      <Root>
        <MainRoute />
        <FlashMessage position="top" />
      </Root>
    );
  }
}
